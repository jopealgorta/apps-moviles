const mongoose = require('mongoose');

const personSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'A person must have a name']
	},
	lastname: {
		type: String,
		required: [true, 'A person must have a lastname']
	},
	age: {
		type: Number,
		required: [true, 'A person must have an age']
	},
	createdAt: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('Person', personSchema);
