const express = require('express');
const mongoose = require('mongoose');
const Person = require('./models/person');
const morgan = require('morgan');

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(express.static(`${__dirname}/public`));

mongoose
	.connect('mongodb://localhost:27017/personas', {
		useUnifiedTopology: true,
		useNewUrlParser: true,
		useFindAndModify: false
	})
	.then(db => console.log('DB connected!'));

app.get('/people', async (req, res) => {
	try {
		const people = await Person.find().sort({ createdAt: -1 });
		res.status(200).json(people);
	} catch (err) {
		res.status(400).json({ message: err.message });
	}
});

app.post('/people', async (req, res) => {
	try {
		const newPerson = await Person.create(req.body);

		// res.redirect('/');
		// res.status(200).json({ newPerson });
		res.status(201).send();
	} catch (err) {
		res.status(404).json({ message: err.message });
	}
});

app.delete('/people/:id', async (req, res) => {
	try {
		await Person.findByIdAndDelete(req.params.id);
		res.status(201).send();
	} catch (err) {
		res.status(404).end();
	}
});

app.put('/people/:id', async (req, res) => {
	try {
		await Person.findByIdAndUpdate(req.params.id, req.body, {
			new: true,
			runValidators: true
		});
		res.status(200).send();
	} catch (err) {
		res.status(404).send();
	}
});

app.delete('/people', async (req, res) => {
	try {
		await Person.deleteMany();
		res.status(204).send();
	} catch (err) {}
});

var port = process.env.PORT || 3000;
app.listen(port, function () {
	console.log('listening on port:', port);
});
