var num = 0;
var inNumber = true;
var positive = true;
var afterRes = false;
var decimalPart = false;
var newNumber = 0;
var newNumberIndex = 0;
var exp = '';
document.calc.disp.value = 0;

function ins(a) {
    $('.clear').html('<span>C</span>');
    if (afterRes) {
        document.calc.disp.value = a;
        inNumber = true;
        afterRes = false;
        newNumber = 0;
        exp = '';
    } else {
        if (inNumber) {
            if (document.calc.disp.value == '0') {
                if (a == '.') document.calc.disp.value = '0' + a;
                else document.calc.disp.value = a;
            } else if (document.calc.disp.value == '-0') {
                if (a == '.') document.calc.disp.value = '-0.' + a;
                else document.calc.disp.value = '-' + a;
            } else {
                if (a != '.') document.calc.disp.value += a;
                else if (!decimalPart) {
                    document.calc.disp.value += a;
                    decimalPart = true;
                } 
            }    
        } else {
            if (a == '.') document.calc.disp.value = '0.';
            else document.calc.disp.value = a;
            inNumber = true;
        }
    }
    console.log('exp ' + exp);
    console.log('num ' + newNumber);
}

function op(a) {
    if (afterRes) {
        exp = document.calc.disp.value + a;
        inNumber = false;
        afterRes = false;
    } else {
        if (inNumber) {
            newNumber = Number(document.calc.disp.value);
            exp += newNumber;
            inNumber = false;
            exp += a;
        } else {
            exp = exp.slice(0, -1);
            exp += a;
        }
    }
    newNumberIndex = exp.length - 1;
    positive = true;
    console.log('num ' + newNumber);
    newNumber = 0;
    decimalPart = false;
    $(document).ready(function() {
        $(".op").click(function () {
            $(this).addClass("active");
            $(".op").not(this).removeClass("active");
        });
    });
    console.log('exp ' + exp);
}

function res() {
    newNumber = Number(document.calc.disp.value);
    exp += newNumber;
    var res = eval(exp);
    console.log('res ' + res);
    exp = '';
    exp += res;
    if (res == 'Infinity' || res == 'NaN') res = 'Error';
    $('.active').removeClass('active');
    if (res % 1 != 0) document.calc.disp.value = res.toFixed(5);
    else document.calc.disp.value = res;
    newNumber = Number(res);
    afterRes = true;
    decimalPart = false;
    console.log('exp ' + exp);
    console.log('num ' + newNumber);
}

function clean() {
    $('.clear').html('<span>AC</span>');
    exp = '';
    document.calc.disp.value = 0;
    $('.active').removeClass('active');
    newNumber = 0;
    positive = true;
    decimalPart = false;
}

function sign() {
    newNumber = Number(document.calc.disp.value);
    if (newNumber == 0) {
        if (document.calc.disp.value == '0') {
            document.calc.disp.value = '-0';
        } else
            document.calc.disp.value = '0';
    } else
        document.calc.disp.value *= -1;
    newNumber *= -1;
    console.log('num ' + newNumber);
    console.log('exp ' + exp);
}

function per() {
    document.calc.disp.value /= 100;
    // newNumber /= 100;
    // console.log('num ' + newNumber);
    console.log('exp ' + exp);
}