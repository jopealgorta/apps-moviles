$(function () {
	const getPeople = () => {
		$.ajax({
			type: 'GET',
			url: '/people',
			dataType: 'json',
			success: function (people) {
				$('.people-list ul').empty();
				people.forEach(p => {
					$('.people-list ul').append(replaceLi(p));
				});
			}
		});
	};

	getPeople();

	$(document).on('click', '#updatePerson', function () {
		var person = $(this).closest('li').find('.info');
		var personName = person.find('.name').html();
		var personLname = person.find('.lastname').html();
		var personAge = person.find('span span').html();
		var personID = $(this).siblings('#personID').html();
		$('#peopleForm #name').val(personName);
		$('#peopleForm #lastname').val(personLname);
		$('#peopleForm #age').val(personAge);
		$('#peopleForm #id').html(personID);
		$('#peopleForm #addPeople').html('Update');
	});

	const deleteAllPeople = () => {
		var result = confirm('Estas seguro de eliminar a todas las personas?');
		if (result) {
			$.ajax({
				type: 'DELETE',
				url: '/people',
				success: function (response) {
					getPeople();
				}
			});
		}
	};

	const addPeople = () => {
		$.ajax({
			type: 'POST',
			url: '/people',
			data: {
				name: $('#name').val(),
				lastname: $('#lastname').val(),
				age: $('#age').val()
			},
			success: function (response) {
				getPeople();
				resetForm();
			}
		});
	};

	$('#getPeople').click(function () {
		getPeople();
	});

	$('#deleteAllPeople').click(function () {
		deleteAllPeople();
	});

	$(document).on('click', '#deletePerson', function () {
		var result = confirm('Estas seguro de eliminar esta persona?');
		if (result) {
			$.ajax({
				type: 'DELETE',
				url: `/people/${$(this).siblings('#personID').html()}`,
				success: function (response) {
					getPeople();
				}
			});
		}
	});

	$('#peopleForm').submit(function (e) {
		e.preventDefault();
		if ($('#peopleForm #id').html() == '') {
			addPeople();
		} else {
			$.ajax({
				type: 'PUT',
				url: `/people/${$('#id').html()}`,
				data: {
					name: $('#name').val(),
					lastname: $('#lastname').val(),
					age: $('#age').val()
				},
				success: function (response) {
					getPeople();
					resetForm();
				}
			});
		}
	});

	const replaceLi = person => {
		return `<li>
		<div class="info">
		<h3 class="name">${person.name}</h3>&nbsp;<h3 class="lastname">${person.lastname}</h3>
		<span><span>${person.age}</span> años</span>
		</div>
		<div class="actions">
		<span id="personID" hidden>${person._id}</span>
		<button id="deletePerson" class="btn btn-danger">
		<i class="fa fa-trash"></i>
		</button>
		<button id="updatePerson" class="btn btn-info"><i class="fa fa-edit"></i></button>
		</div>
		</li>`;
	};

	const resetForm = () => {
		$('#peopleForm #name').val('');
		$('#peopleForm #lastname').val('');
		$('#peopleForm #age').val('');
		$('#peopleForm #id').html('');
		$('#peopleForm #addPeople').html('Create');
	};
});
