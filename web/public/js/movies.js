$(document).ready(function () {
    $(".loader-wrapper").fadeOut();

    $('#search-movie').change(function (e) { 
        e.preventDefault();
        consultarCartelera($(this).children("option:selected").val())
    });

    $(".nav__btn").click(function () {
        $(this).addClass("active");
        $(".nav__btn").not(this).removeClass("active");
    });
});


function createMovieHtml (data) {
    var movieHtml = '<div class="movie" onclick="openPopup(\'' + data.movie + '\', 1)">' + 
                        '<img src="' + data.posterURL + '" alt="' + data.movie + '" class="movie__img">' + 
                        '<h6 class="movie__title">' + data.movie + '</h6>'
                    '</div>';
    return movieHtml;
}

function consultarCartelera(cine, btn) {
    $(btn).html('<i class="fa fa-spinner fa-spin"></i> Ver Cartelera');
    $(".loader-wrapper").fadeIn("fast");
    $.ajax({
        type: "GET",
        url: "https://api.movie.com.uy/api/shows/rss/data",
        crossDomain: true,
        data: "data",
        dataType: "json",
        success: function (response) {
            $('.movies').empty();
            $(".loader-wrapper").fadeOut();
            $(btn).text('Ver cartelera');
            for (const movie in response.contentCinemaShows) {
                const element = response.contentCinemaShows[movie];
                var hasMovie = false;
                for (const show in element.cinemaShows) {
                    const sh = element.cinemaShows[show];
                    if (sh.cinema == cine) {
                        hasMovie = true;
                        $('.movies').append(createMovieHtml(element));
                        $('.movies').append(createMovieHtml(element));
                        $('.movies').append(createMovieHtml(element));
                        $('.movies').append(createMovieHtml(element));
                        $('.movies').append(createMovieHtml(element));
                        $('.movies').append(createMovieHtml(element));
                    }
                }
            }
            if (hasMovie == false) {
                $('.movies').append('<div class="movies__msg">No se encontraron resultados para <b>' + cine + '</b></div>');
            }
            $('html, body').animate({
                scrollTop: $("#section-movies").offset().top - 120
            }, 600);
        },
        failure: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}

function scroll_ (to, pad = 0) {
    $('html, body').animate({
        scrollTop: $("#" + to).offset().top + pad
    }, 600);
}

function closePopup(num) {
    $('#popup' + num).css({
        "opacity": 0,
        "visibility": "hidden"
    });
    $('#popup' + '.popup__content').css({ 
        "opacity": 0,
        "transform": "translate(-50%, -50%) scale(.2)"
    });
}

function openPopup(movieName, num) {
    $.ajax({
        type: "GET",
        url: "https://api.movie.com.uy/api/shows/rss/data",
        crossDomain: true,
        data: "data",
        dataType: "json",
        success: function (response) {
            for (const movie in response.contentCinemaShows) {
                const element = response.contentCinemaShows[movie];
                if (element.movie == movieName) {
                    $('#popup' + num).css({
                        "opacity": 1,
                        "visibility": "visible"
                    });
                    $('.popup__content').css({
                        "opacity": 1,
                        "transform": "translate(-50%, -50%) scale(1)"
                    });
                    if (num == 1) {
                        $('#popup' + num + ' .popup__left img').attr("src", element.posterURL);
                        $('#popup' + num + ' .popup__sec #movie').text(element.movie);
                        $('#popup' + num + ' .popup__sec #description').text(element.description);
                        $('#popup' + num + ' .popup__sec #genre').text(element.genre);
                        $('#popup' + num + ' .shows').attr('onclick', 'openPopup(\'' + element.movie + '\', 2)')
                    } else {
                        console.log(element.cinemaShows[0].shows);
                        $('#popup' + num + ' .popup__left img').attr("src", element.posterURL);
                        $('#popup' + num + ' .popup__right.popup__right--scroll .shows').empty();
                        element.cinemaShows.forEach(shows => {
                            shows.shows.forEach(show => {
                                console.log(show);
                                $('#popup' + num + ' .popup__right.popup__right--scroll .shows').append(createShowHtml(show));
                            });
                        });
                    }
                }
            }
        },
        failure: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}

function createShowHtml(data) {
    var showHtml =  `<div class="popup__sec popup__sec--2">
                        <h6 class="popup__heading--2">Fecha y Hora:</h6>${data.timeToDisplay} 
                    </div> 
                    <div class="popup__sec popup__sec--2"> 
                        <h6 class="popup__heading--2">Sala:</h6>${data.screenName} 
                    </div>
                    <div class="popup__sec popup__sec--2"> 
                        <h6 class="popup__heading--2">Formato:</h6>${data.formatLang} 
                    </div>
                    <div class="popup__sec popup__sec--2"> 
                        <h6 class="popup__heading--2 rating">${data.ratingDescription}</h6> 
                    </div><br><hr>`;
    return showHtml;
}