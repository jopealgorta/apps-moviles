const { getData } = require('./httpbase');
const http = require('http');
const { MongoClient } = require('mongodb');
const uri = 'mongodb://localhost:27017/';

(async () => {
	const client = await MongoClient.connect(uri, { useUnifiedTopology: true });
	const db = await client.db('personas');

	const server = http.createServer();
	server.on('request', (req, res) => {
		getData(db).then(data => {
			const nData = JSON.stringify(data, null, 4);
			console.log(nData);
			res.end(nData);
		});
	});
	server.listen(3000, () => {
		console.log('Listening on 3000');
	});
})();