const getData = async (database) => {
	try {
		return await database
			.collection('personas')
			.find({}, { projection: { _id: 0 } })
			.toArray();
	} catch (error) {
		throw error;
	}
};
module.exports = { getData };
